Xonotic
=======

Clone
-----
    git clone git@gitlab.com:xonotic/daemon.git
    cd daemon
    git submodule update --init --recursive

Build
-----

    cmake -H. -Bbuild -G "Unix Makefiles"
    cmake --build build -- -j $(nproc)

Setup
-----

    sudo pip install cogapp

[Creating a xonotic_1.pk3dir package](https://wiki.unvanquished.net/index.php?title=Filesystem#Creating_your_own_package)

Run
---
    ./daemon \
    -set vm.cgame.type 3 -set vm.sgame.type 3 \
    -pakpath ./data \
    -set fs_legacypaks 1 \
    -set fs_extrapaks "xonotic-data xonotic-maps xonotic-nexcompat" \
    +devmap solarium

Config
------

    cl_consoleCommand ""
    listCvars *pattern*
    r_customwidth 1280
    r_customheight 720
    r_allowResize 1
    logs.logLevel.sgame.default "debug"
