#pragma once

#include <functional>
#include <map>
#include <memory>

namespace meta {

    template<int... I>
    struct index_tuple_type {
        template<int N>
        using append = index_tuple_type<I..., N>;
    };

    template<int N>
    struct make_index_impl {
        using type = typename make_index_impl<N - 1>::type::template append<N - 1>;
    };

    template<>
    struct make_index_impl<0> {
        using type = index_tuple_type<>;
    };

    template<class I, class... Args>
    struct func_traits;

    template<class R, int... I, class... Args>
    struct func_traits<R, index_tuple_type<I...>, Args...> {
        template<class T, class F>
        static inline R call(F f, T t)
        {
            (void) t;
            return f(std::get<I>(t)...);
        }
    };

    template<class F, class... Args>
    void sequential_foreach(F f, const Args &... args)
    {
        (void) std::initializer_list<int>{((void) f(args), 0)...};
    }

#define VARIANT(T)                                                                                          \
        template<class F, class... Args, class R = typename std::result_of<F(Args...)>::type>               \
        inline R explode(F f, T t)                                                                          \
        {                                                                                                   \
            return func_traits<R, typename make_index_impl<sizeof...(Args)>::type, Args...>::call(f, t);    \
        }

    VARIANT(std::tuple<Args...> &)

    VARIANT(std::tuple<Args...> &&)

    VARIANT(const std::tuple<Args...> &)

#undef VARIANT

    template<class T, class E>
    T E::*offset_to_member(int offset)
    {
        static_assert(sizeof(T E::*) == sizeof(void *), "No funny business");
        union {
            T E::*pmember;
            struct {
                int i, _;
            };
        } x;
        x.i = offset;
        x._ = 0;
        return x.pmember;
    };

    template<class T, class E>
    int member_to_offset(T E::* ptr)
    {
        static_assert(sizeof(T E::*) == sizeof(void *), "No funny business");
        union {
            T E::*pmember;
            struct {
                int i, _;
            };
        } x;
        x.pmember = ptr;
        return x.i;
    };

}
