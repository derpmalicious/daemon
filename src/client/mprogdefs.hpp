struct globalvars_t {
    int pad[28];
    int self;
};

struct entvars_t {
    entvars_t &operator=(const entvars_t &) = delete;

    entvars_t(const entvars_t &) = delete;
};
