#include <engine/server/sg_api.h>
#include <engine/server/sg_msgdef.h>

#include <shared/CommonProxies.h>
#include <shared/VMMain.h>
#include <shared/server/sg_api.h>

#include "commands.hpp"
#include <vm/Q1VM.hpp>
#include <vm/sys/server.hpp>
#include "cvars.hpp"

static Q1VM *q1vm;

struct entity {

};

struct gentity_t {
    sharedEntity_t engineState;
    entity data;
};

struct gclient_t {
    playerState_t engineState;
};

gentity_t *entities;
gclient_t *clients;

void LocateGameData(int numGEntities, int sizeofGEntity, int sizeofGClient)
{
    static bool firstTime = true;
    if (firstTime) {
        VM::SendMsg<LocateGameDataMsg1>(shmRegion, numGEntities, sizeofGEntity, sizeofGClient);
        firstTime = false;
    } else {
        VM::SendMsg<LocateGameDataMsg2>(numGEntities, sizeofGEntity, sizeofGClient);
    }
}

void VM::VMInit()
{
    shmRegion = IPC::SharedMemory::Create(sizeof(*entities) * MAX_GENTITIES + sizeof(*clients) * MAX_CLIENTS);
    uint8_t *shmBase = reinterpret_cast<uint8_t *>(shmRegion.GetBase());
    entities = reinterpret_cast<gentity_t *>(shmBase);
    clients = reinterpret_cast<gclient_t *>(shmBase + sizeof(*entities) * MAX_GENTITIES);

    LocateGameData(MAX_CLIENTS, sizeof(*entities), sizeof(*clients));

    const auto mapname = Cvar::GetValue("mapname");
    trap_CM_LoadMap(mapname);
}

void spawn(const std::string className, Entity e = q1vm->entityManager.spawn())
{
    q1vm->globalVars.self = e.id;
    q1vm->exec("SV_OnEntityPreSpawnFunction");
    if (!q1vm->exec(("spawnfunc_" + className).c_str())) {
        q1vm->exec("SV_OnEntityNoSpawnFunction");
    }
    q1vm->exec("SV_OnEntityPostSpawnFunction");
}

void VM::VMHandleSyscall(uint32_t id, Util::Reader reader)
{
    const int major = id >> 16;
    const auto minor = static_cast<gameExport_t>(id & 0xFFFF);
    if (major == VM::QVM) {
        switch (minor) {
            default:
                Sys::Error("VM::VMHandleSyscall: unknown sgame command %i", minor);
            case GAME_STATIC_INIT:
                IPC::HandleMsg<GameStaticInitMsg>(VM::rootChannel, std::move(reader), [](int milliseconds) {
                    VM::InitializeProxies(milliseconds);
                    FS::Initialize();
                    VM::VMInit();
                });
                break;
            case GAME_INIT: {
                auto func = [](int levelTime, int randomSeed, bool cheats, bool inClient) {
                    Q_UNUSED(levelTime);
                    Q_UNUSED(randomSeed);
                    Q_UNUSED(cheats);
                    Q_UNUSED(inClient);
                    Cvars::init();
                    Commands::init();
                    q1vm = Q1VM::load(Q1VM::type::sv, "progs.dat");
                    spawn("worldspawn", q1vm->entityManager.get(0));
                };
                IPC::HandleMsg<GameInitMsg>(VM::rootChannel, std::move(reader), func);
                break;
            }
            case GAME_SHUTDOWN: {
                auto func = [](bool restart) {
                    Q_UNUSED(restart);
                };
                IPC::HandleMsg<GameShutdownMsg>(VM::rootChannel, std::move(reader), func);
                break;
            }
            case GAME_CLIENT_CONNECT: {
                auto func = [](int clientNum, bool firstTime, int isBot, bool &denied, std::string &reason) {
                    Q_UNUSED(clientNum);
                    Q_UNUSED(firstTime);
                    Q_UNUSED(isBot);
                    Q_UNUSED(denied);
                    Q_UNUSED(reason);
                };
                IPC::HandleMsg<GameClientConnectMsg>(VM::rootChannel, std::move(reader), func);
                break;
            }
            case GAME_CLIENT_BEGIN: {
                auto func = [](int clientNum) {
                    Q_UNUSED(clientNum);
                };
                IPC::HandleMsg<GameClientBeginMsg>(VM::rootChannel, std::move(reader), func);
                break;
            }
            case GAME_CLIENT_USERINFO_CHANGED: {
                auto func = [](int clientNum) {
                    Q_UNUSED(clientNum);
                };
                IPC::HandleMsg<GameClientUserinfoChangedMsg>(VM::rootChannel, std::move(reader), func);
                break;
            }
            case GAME_CLIENT_DISCONNECT: {
                auto func = [](int clientNum) {
                    Q_UNUSED(clientNum);
                };
                IPC::HandleMsg<GameClientDisconnectMsg>(VM::rootChannel, std::move(reader), func);
                break;
            }
            case GAME_CLIENT_COMMAND: {
                auto func = [](int clientNum, std::string command) {
                    Q_UNUSED(clientNum);
                    Q_UNUSED(command);
                };
                IPC::HandleMsg<GameClientCommandMsg>(VM::rootChannel, std::move(reader), func);
                break;
            }
            case GAME_CLIENT_THINK: {
                auto func = [](int clientNum) {
                    Q_UNUSED(clientNum);
                };
                IPC::HandleMsg<GameClientThinkMsg>(VM::rootChannel, std::move(reader), func);
                break;
            }
            case GAME_RUN_FRAME: {
                auto func = [](int levelTime) {
                    Q_UNUSED(levelTime);
                    Cvars::update();
                    q1vm->exec("StartFrame");
                };
                IPC::HandleMsg<GameRunFrameMsg>(VM::rootChannel, std::move(reader), func);
                break;
            }
            case GAME_SNAPSHOT_CALLBACK:
                Sys::Error("GAME_SNAPSHOT_CALLBACK not implemented");
            case BOTAI_START_FRAME:
                Sys::Error("BOTAI_START_FRAME not implemented");
            case BOT_VISIBLEFROMPOS:
                Sys::Error("BOT_VISIBLEFROMPOS not implemented");
            case BOT_CHECKATTACKATPOS:
                Sys::Error("BOT_CHECKATTACKATPOS not implemented");
            case GAME_MESSAGERECEIVED:
                Sys::Error("GAME_MESSAGERECEIVED not implemented");
        }
    } else if (major < VM::LAST_COMMON_SYSCALL) {
        VM::HandleCommonSyscall(major, minor, std::move(reader), VM::rootChannel);
    } else {
        Sys::Error("VM::VMHandleSyscall: unhandled VM major syscall number %i", major);
    }
}
