#pragma once

#include <memory>
#include <string>

class QStr {
public:

    QStr(std::string const *s);

    QStr(std::string const s);

    QStr(char const *c);

    operator std::shared_ptr<std::string const>() const;

    std::string const &operator*() const;

    operator std::string const &() const;

    std::shared_ptr<std::string const> operator->() const;

private:
    // TODO: replace with uint32 handle
    std::shared_ptr<std::string const> const s;
};

inline bool operator==(QStr a, QStr b)
{
    return *a == *b;
}

inline bool operator==(QStr s, const char *lit)
{
    return s == QStr(lit);
}
