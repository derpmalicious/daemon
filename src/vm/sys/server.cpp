#include "../Q1VM.hpp"

#include "com/Buffer.hpp"
#include <engine/server/sg_api.h>
#include <engine/client/cg_api.h> // FIXME

#include <cmath>
#include <engine/qcommon/q_shared.h>
#include <common/cm/cm_public.h>

#ifndef func_t
    #define func_t int
    #define string_t int
    #include <server/progdefs.hpp>

#else
    #undef func_t
    #undef string_t
#endif

#ifndef BUILTIN
#include "server.hpp"

#undef BUILTIN
#define BUILTIN(vm, number, ret, id, params) \
    ret id params; auto _builtin_##id = Q1VM::Builtin<Q1VM::type::vm, number, Q1VM_MAKE_BUILTIN(id)>(); \
    ret id params
#endif

/*[[[cog
import cog_builtins
cog_builtins.parse('src/vm/sys/common.yml', 'sv')
]]]*/
namespace sv {
    using entity = edict_s&;
    using string = QStr;
    using vector = QVec3;
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wreturn-type"
#pragma GCC diagnostic ignored "-Wunused-parameter"
    BUILTIN(sv, 1, void, makevectors, (vector ang)) {
         vec3_t in, f, r, u; ang.Store(in); AngleVectors(in, f, r, u); thisvm->globalVars.v_forward = f; thisvm->globalVars.v_right = r; thisvm->globalVars.v_up = u; ;
    }
    BUILTIN(sv, 2, void, setorigin, (entity e, vector o)) {
        e.origin = o;;
    }
    BUILTIN(sv, 3, void, setmodel, (entity e, string m)) {
        /* TODO */;
    }
    BUILTIN(sv, 4, void, setsize, (entity e, vector min, vector max)) {
        /* TODO */;
    }
    BUILTIN(sv, 7, float, _random, ()) {
        return random();
    }
    BUILTIN(sv, 9, vector, normalize, (vector v)) {
        return v.Normalized();
    }
    BUILTIN(sv, 10, void, error, (string s)) {
        Sys::Error(s->c_str());
    }
    BUILTIN(sv, 12, float, vlen, (vector v)) {
        return v.Length();
    }
    BUILTIN(sv, 13, float, vectoyaw, (vector v)) {
         if (v.x == 0 && v.y == 0) return 0; float yaw = std::atan2(v.y, v.x) * 180 / M_PI; if (yaw < 0) yaw += 360; return yaw ;
    }
    BUILTIN(sv, 14, entity, spawn, ()) {
        return thisvm->spawn().data;
    }
    BUILTIN(sv, 15, void, remove, (entity e)) {
        /* TODO */;
    }
    BUILTIN(sv, 18, entity, find, (entity start, /* string */ int edict_s::* fld, string match)) {
        return thisvm->entityManager.find(start, fld, match);
    }
    BUILTIN(sv, 19, void, precache_sound, (string s)) {
        /* TODO */;
    }
    BUILTIN(sv, 20, void, precache_model, (string s)) {
        /* TODO */;
    }
    BUILTIN(sv, 23, void, bprint, (string s)) {
        /* TODO */;
    }
    BUILTIN(sv, 25, void, dprint, (string s)) {
        Log::Verbose("%s", s->c_str());
    }
    BUILTIN(sv, 26, string, ftos, (float f)) {
        return (static_cast<int>(f) == f) ? va("%.0f", f) : va("%f", f);
    }
    BUILTIN(sv, 35, void, lightstyle, (float style, string value)) {
        /* TODO */;
    }
    BUILTIN(sv, 36, float, rint, (float f)) {
        return std::rint(f);
    }
    BUILTIN(sv, 37, float, floor, (float f)) {
        return std::floor(f);
    }
    BUILTIN(sv, 38, float, ceil, (float f)) {
        return std::ceil(f);
    }
    BUILTIN(sv, 43, float, fabs, (float f)) {
        return std::fabs(f);
    }
    BUILTIN(sv, 45, float, cvar, (string s)) {
        return 0/* TODO */;
    }
    BUILTIN(sv, 46, void, localcmd, (string s)) {
        /* TODO */;
    }
    BUILTIN(sv, 47, entity, nextent, (entity e)) {
        return thisvm->entityManager.next(e);
    }
    BUILTIN(sv, 51, vector, vectoangles, (vector v)) {
        return {0, 0, 0}/* TODO */;
    }
    BUILTIN(sv, 60, float, sin, (float f)) {
        return std::sin(f);
    }
    BUILTIN(sv, 61, float, cos, (float f)) {
        return std::cos(f);
    }
    BUILTIN(sv, 62, float, sqrt, (float f)) {
        return std::sqrt(f);
    }
    BUILTIN(sv, 64, void, tracetoss, (entity e, entity ignore)) {
        /* TODO */;
    }
    BUILTIN(sv, 65, string, etos, (entity ent)) {
        return Str::Format("entity %i", EntityManager::identify(ent));
    }
    BUILTIN(sv, 70, void, changelevel, (string s)) {
        /* TODO */;
    }
    BUILTIN(sv, 72, void, cvar_set, (string var, string val)) {
        /* TODO */;
    }
    BUILTIN(sv, 81, float, stof, (string s)) {
        try { return std::stof(s); } catch (std::invalid_argument &e) { return 0; };
    }
    BUILTIN(sv, 90, void, tracebox, (vector v1, vector min, vector max, vector v2, float nomonsters, entity forent)) {
         trace_t tr; vec3_t start; v1.Store(start); vec3_t end;   v2.Store(end); vec3_t mins;  min.Store(mins); vec3_t maxs;  max.Store(maxs); CM_BoxTrace(&tr, start, end, mins, maxs, 0, -1, 0, traceType_t::TT_AABB); thisvm->globalVars.trace_allsolid = tr.allsolid; thisvm->globalVars.trace_endpos = tr.endpos; thisvm->globalVars.trace_ent = tr.entityNum; thisvm->globalVars.trace_fraction = tr.fraction; thisvm->globalVars.trace_startsolid = tr.startsolid; thisvm->globalVars.trace_plane_dist = tr.plane.dist; thisvm->globalVars.trace_plane_normal = tr.plane.normal; /* tr.contents; */ /* tr.lateralFraction; */ /* tr.surfaceFlags; */ ;
    }
    BUILTIN(sv, 93, float, registercvar, (string name, string value)) {
        return 0/* TODO */;
    }
    BUILTIN(sv, 94, float, min, (float a, float b)) {
        return std::min(a, b)/* TODO */;
    }
    BUILTIN(sv, 95, float, max, (float a, float b)) {
        return std::max(a, b)/* TODO */;
    }
    BUILTIN(sv, 96, float, bound, (float low, float x, float high)) {
        return std::min(std::max(low, x), high);
    }
    BUILTIN(sv, 97, float, pow, (float a, float b)) {
        return std::pow(a, b);
    }
    BUILTIN(sv, 98, entity, findfloat, (entity start, float edict_s::* fld, float match)) {
        return thisvm->entityManager.find(start, fld, match);
    }
    BUILTIN(sv, 99, float, checkextension, (string s)) {
        return 1/* TODO */;
    }
    BUILTIN(sv, 110, float, fopen, (string filename, float mode)) {
         fileHandle_t fd; trap_FS_FOpenFile(filename->c_str(), &fd, mode == 2 ? fsMode_t::FS_WRITE : mode == 1 ? fsMode_t::FS_APPEND : fsMode_t::FS_WRITE ); return fd ;
    }
    BUILTIN(sv, 111, void, fclose, (float fhandle)) {
        /* TODO */;
    }
    BUILTIN(sv, 112, string, fgets, (float fhandle)) {
        return ""/* TODO */;
    }
    BUILTIN(sv, 113, void, fputs, (float fhandle, string s)) {
        /* TODO */;
    }
    BUILTIN(sv, 114, float, strlen, (string s)) {
        return s->length();
    }
    BUILTIN(sv, 115, string, strcat, (Q1VM::Arguments args)) {
        std::string ret; for (int i = 0; i < args.size(); ++i) ret += args.get<string>(i); return ret;
    }
    BUILTIN(sv, 116, string, substring, (string s, float start, float length)) {
        try { return s->substr(start, length); } catch(std::out_of_range e) { return ""; };
    }
    BUILTIN(sv, 117, vector, stov, (string)) {
        return {0, 0, 0}/* TODO */;
    }
    BUILTIN(sv, 118, string, strzone, (string s)) {
        return s;
    }
    BUILTIN(sv, 119, void, strunzone, (string s)) {
        ;
    }
    BUILTIN(sv, 221, float, strstrofs, (string str, string sub, float startpos)) {
         const auto pos = str->find(sub, startpos); if (pos == std::string::npos) return -1; return pos;
    }
    BUILTIN(sv, 225, string, strpad, (float chars, string s)) {
        return s/* TODO */;
    }
    BUILTIN(sv, 226, string, infoadd, (string info, string key, string value)) {
        return ""/* TODO */;
    }
    BUILTIN(sv, 227, string, infoget, (string info, string key)) {
        return ""/* TODO */;
    }
    BUILTIN(sv, 228, float, strncmp, (string s1, string s2, float len)) {
        return s1->compare(s2) == 0/* TODO */;
    }
    BUILTIN(sv, 229, float, strcasecmp, (string s1, string s2)) {
        return s1->compare(s2) == 0/* TODO */;
    }
    BUILTIN(sv, 230, float, strncasecmp, (string s1, string s2, float len)) {
        return s1->compare(s2) == 0/* TODO */;
    }
    BUILTIN(sv, 232, void, addstat, (float index, float type, float edict_s::* field)) {
        /* TODO */;
    }
    BUILTIN(sv, 277, float, frameduration, (float modlindex, float framenum)) {
        return 0/* TODO */;
    }
    BUILTIN(sv, 339, void, print, (Q1VM::Arguments args)) {
        Log::Notice("%s", strcat(args)->c_str());
    }
    BUILTIN(sv, 400, void, copyentity, (entity from, entity to)) {
        thisvm->entityManager.copyEntity(from, to);
    }
    BUILTIN(sv, 402, entity, findchain, (string edict_s::* fld, string match, Q1VM::Arguments args)) {
        return thisvm->entityManager.findChain(fld, match, (args.size() == 0) ? &edict_s::chain : args.get<int edict_s::*>(0));
    }
    BUILTIN(sv, 403, entity, findchainfloat, (float edict_s::* fld, float match, Q1VM::Arguments args)) {
        return thisvm->entityManager.findChain(fld, match, (args.size() == 0) ? &edict_s::chain : args.get<int edict_s::*>(0));
    }
    BUILTIN(sv, 442, string, argv, (float n)) {
        return ""/* TODO */;
    }
    BUILTIN(sv, 444, float, search_begin, (string pattern, float caseinsensitive, float quiet)) {
        return 0/* TODO */;
    }
    BUILTIN(sv, 445, void, search_end, (float handle)) {
        /* TODO */;
    }
    BUILTIN(sv, 446, float, search_getsize, (float handle)) {
        return 0/* TODO */;
    }
    BUILTIN(sv, 447, string, search_getfilename, (float handle, float num)) {
        return ""/* TODO */;
    }
    BUILTIN(sv, 448, string, cvar_string, (string s)) {
        return "0"/* TODO */;
    }
    BUILTIN(sv, 451, float, gettagindex, (entity ent, string tagname)) {
        return 0/* TODO */;
    }
    BUILTIN(sv, 459, entity, entitybyindex, (float num)) {
        return thisvm->entityManager.get(num).data;
    }
    BUILTIN(sv, 460, float, buf_create, ()) {
        return Buffer::New();
    }
    BUILTIN(sv, 461, void, buf_del, (float bufhandle)) {
        Buffer::Delete(static_cast<int>(bufhandle));
    }
    BUILTIN(sv, 462, float, buf_getsize, (float bufhandle)) {
        /* TODO */;
    }
    BUILTIN(sv, 463, void, buf_copy, (float bufhandle_from, float bufhandle_to)) {
        /* TODO */;
    }
    BUILTIN(sv, 464, void, buf_sort, (float bufhandle, float sortpower, float backward)) {
        /* TODO */;
    }
    BUILTIN(sv, 466, string, bufstr_get, (float bufhandle, float string_index)) {
        return "TODO"/* TODO */;
    }
    BUILTIN(sv, 467, void, bufstr_set, (float bufhandle, float string_index, string str)) {
        /* TODO */;
    }
    BUILTIN(sv, 468, float, bufstr_add, (float bufhandle, string str, float order)) {
        return 0/* TODO */;
    }
    BUILTIN(sv, 469, void, bufstr_free, (float bufhandle, float string_index)) {
        /* TODO */;
    }
    BUILTIN(sv, 471, float, asin, (float f)) {
        return std::asin(f);
    }
    BUILTIN(sv, 472, float, acos, (float f)) {
        return std::acos(f);
    }
    BUILTIN(sv, 473, float, atan, (float f)) {
        return std::atan(f);
    }
    BUILTIN(sv, 474, float, atan2, (float a, float b)) {
        return std::atan2(a, b);
    }
    BUILTIN(sv, 475, float, tan, (float f)) {
        return std::tan(f);
    }
    BUILTIN(sv, 479, float, tokenizebyseparator, (string s)) {
        return 0/* TODO */;
    }
    BUILTIN(sv, 480, string, strtolower, (string s)) {
         std::string ret; std::locale loc; for (auto i = 0u; i < s->length(); ++i) ret += std::tolower(s->at(i), loc); return ret;
    }
    BUILTIN(sv, 481, string, strtoupper, (string s)) {
         std::string ret; std::locale loc; for (auto i = 0u; i < s->length(); ++i) ret += std::toupper(s->at(i), loc); return ret;
    }
    BUILTIN(sv, 482, string, cvar_defstring, (string s)) {
        return "0"/* TODO */;
    }
    BUILTIN(sv, 484, string, strreplace, (string search, string replace, string subject)) {
        return subject/* TODO */;
    }
    BUILTIN(sv, 485, string, strireplace, (string search, string replace, string subject)) {
        return subject/* TODO */;
    }
    BUILTIN(sv, 494, float, crc16, (float caseinsensitive, string s)) {
        return 0/* TODO */;
    }
    BUILTIN(sv, 495, float, cvar_type, (string name)) {
         const float CVAR_TYPEFLAG_EXISTS = BIT(0); const float CVAR_TYPEFLAG_SAVED = BIT(1); const float CVAR_TYPEFLAG_PRIVATE = BIT(2); const float CVAR_TYPEFLAG_ENGINE = BIT(3); const float CVAR_TYPEFLAG_HASDESCRIPTION = BIT(4); const float CVAR_TYPEFLAG_READONLY = BIT(5); /* TODO */ return CVAR_TYPEFLAG_EXISTS;
    }
    BUILTIN(sv, 496, float, numentityfields, ()) {
        return 0/* TODO */;
    }
    BUILTIN(sv, 503, string, whichpack, (string)) {
        return ""/* TODO */;
    }
    BUILTIN(sv, 510, string, uri_escape, (string in)) {
        return in/* TODO */;
    }
    BUILTIN(sv, 511, string, uri_unescape, (string in)) {
        return in/* TODO */;
    }
    BUILTIN(sv, 512, float, num_for_edict, (entity ent)) {
        return static_cast<float>(EntityManager::identify(ent));
    }
    BUILTIN(sv, 514, float, tokenize_console, (string str)) {
        return 0/* TODO */;
    }
    BUILTIN(sv, 517, void, buf_cvarlist, (float buf, string prefix, string antiprefix)) {
        /* TODO */;
    }
    BUILTIN(sv, 519, float, gettime, (float timer)) {
        return 0/* TODO */;
    }
    BUILTIN(sv, 532, float, log, (float f)) {
        return std::log(f);
    }
    BUILTIN(sv, 627, string, sprintf, (string format, Q1VM::Arguments args)) {
         std::string ret; int i = 0; for (const char *s = format->c_str(); *s; ++s) { if (*s != '%') { ret += *s; } else { s += 1; if (*s == '%') { ret += '%'; } else if (*s == 's') { ret += args.get<string>(i++); } else if (*s == 'f') { ret += std::to_string(args.get<float>(i++)); } else {
} } } return ret;
    }
    BUILTIN(sv, 639, string, digest_hex, (string digest, string data)) {
        return data/* TODO */;
    }
#pragma GCC diagnostic pop
}
//[[[end]]] (checksum: 84a59963dbde883499684bc1835302ef)
