#include "../Q1VM.hpp"

#include "com/Buffer.hpp"
#include "com/View.hpp"
#include <engine/client/cg_api.h>

#include <cmath>
#include <engine/qcommon/q_shared.h>
#include <common/cm/cm_public.h>

#ifndef func_t
    #define func_t int
    #define string_t int
    #include <client/clprogdefs.hpp>
#else
    #undef func_t
    #undef string_t
#endif

#ifndef BUILTIN
#include "client.hpp"

#undef BUILTIN
#define BUILTIN(vm, number, ret, id, params) \
    ret id params; auto _builtin_##id = Q1VM::Builtin<Q1VM::type::vm, number, Q1VM_MAKE_BUILTIN(id)>(); \
    ret id params
#endif

/*[[[cog
import cog_builtins
cog_builtins.parse('src/vm/sys/common.yml', 'cl')
]]]*/
namespace cl {
    using entity = edict_s&;
    using string = QStr;
    using vector = QVec3;
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wreturn-type"
#pragma GCC diagnostic ignored "-Wunused-parameter"
    BUILTIN(cl, 1, void, makevectors, (vector ang)) {
         vec3_t in, f, r, u; ang.Store(in); AngleVectors(in, f, r, u); thisvm->globalVars.v_forward = f; thisvm->globalVars.v_right = r; thisvm->globalVars.v_up = u; ;
    }
    BUILTIN(cl, 2, void, setorigin, (entity e, vector o)) {
        e.origin = o;;
    }
    BUILTIN(cl, 3, void, setmodel, (entity e, string m)) {
        /* TODO */;
    }
    BUILTIN(cl, 4, void, setsize, (entity e, vector min, vector max)) {
        /* TODO */;
    }
    BUILTIN(cl, 7, float, _random, ()) {
        return random();
    }
    BUILTIN(cl, 9, vector, normalize, (vector v)) {
        return v.Normalized();
    }
    BUILTIN(cl, 10, void, error, (string s)) {
        Sys::Error(s->c_str());
    }
    BUILTIN(cl, 12, float, vlen, (vector v)) {
        return v.Length();
    }
    BUILTIN(cl, 13, float, vectoyaw, (vector v)) {
         if (v.x == 0 && v.y == 0) return 0; float yaw = std::atan2(v.y, v.x) * 180 / M_PI; if (yaw < 0) yaw += 360; return yaw ;
    }
    BUILTIN(cl, 14, entity, spawn, ()) {
        return thisvm->spawn().data;
    }
    BUILTIN(cl, 15, void, remove, (entity e)) {
        /* TODO */;
    }
    BUILTIN(cl, 18, entity, find, (entity start, /* string */ int edict_s::* fld, string match)) {
        return thisvm->entityManager.find(start, fld, match);
    }
    BUILTIN(cl, 19, void, precache_sound, (string s)) {
        /* TODO */;
    }
    BUILTIN(cl, 20, void, precache_model, (string s)) {
        /* TODO */;
    }
    BUILTIN(cl, 25, void, dprint, (string s)) {
        Log::Verbose("%s", s->c_str());
    }
    BUILTIN(cl, 26, string, ftos, (float f)) {
        return (static_cast<int>(f) == f) ? va("%.0f", f) : va("%f", f);
    }
    BUILTIN(cl, 35, void, lightstyle, (float style, string value)) {
        /* TODO */;
    }
    BUILTIN(cl, 36, float, rint, (float f)) {
        return std::rint(f);
    }
    BUILTIN(cl, 37, float, floor, (float f)) {
        return std::floor(f);
    }
    BUILTIN(cl, 38, float, ceil, (float f)) {
        return std::ceil(f);
    }
    BUILTIN(cl, 43, float, fabs, (float f)) {
        return std::fabs(f);
    }
    BUILTIN(cl, 45, float, cvar, (string s)) {
        return 0/* TODO */;
    }
    BUILTIN(cl, 46, void, localcmd, (string s)) {
        /* TODO */;
    }
    BUILTIN(cl, 47, entity, nextent, (entity e)) {
        return thisvm->entityManager.next(e);
    }
    BUILTIN(cl, 51, vector, vectoangles, (vector v)) {
        return {0, 0, 0}/* TODO */;
    }
    BUILTIN(cl, 60, float, sin, (float f)) {
        return std::sin(f);
    }
    BUILTIN(cl, 61, float, cos, (float f)) {
        return std::cos(f);
    }
    BUILTIN(cl, 62, float, sqrt, (float f)) {
        return std::sqrt(f);
    }
    BUILTIN(cl, 64, void, tracetoss, (entity e, entity ignore)) {
        /* TODO */;
    }
    BUILTIN(cl, 65, string, etos, (entity ent)) {
        return Str::Format("entity %i", EntityManager::identify(ent));
    }
    BUILTIN(cl, 72, void, cvar_set, (string var, string val)) {
        /* TODO */;
    }
    BUILTIN(cl, 81, float, stof, (string s)) {
        try { return std::stof(s); } catch (std::invalid_argument &e) { return 0; };
    }
    BUILTIN(cl, 90, void, tracebox, (vector v1, vector min, vector max, vector v2, float nomonsters, entity forent)) {
         trace_t tr; vec3_t start; v1.Store(start); vec3_t end;   v2.Store(end); vec3_t mins;  min.Store(mins); vec3_t maxs;  max.Store(maxs); CM_BoxTrace(&tr, start, end, mins, maxs, 0, -1, 0, traceType_t::TT_AABB); thisvm->globalVars.trace_allsolid = tr.allsolid; thisvm->globalVars.trace_endpos = tr.endpos; thisvm->globalVars.trace_ent = tr.entityNum; thisvm->globalVars.trace_fraction = tr.fraction; thisvm->globalVars.trace_startsolid = tr.startsolid; thisvm->globalVars.trace_plane_dist = tr.plane.dist; thisvm->globalVars.trace_plane_normal = tr.plane.normal; /* tr.contents; */ /* tr.lateralFraction; */ /* tr.surfaceFlags; */ ;
    }
    BUILTIN(cl, 93, float, registercvar, (string name, string value)) {
        return 0/* TODO */;
    }
    BUILTIN(cl, 94, float, min, (float a, float b)) {
        return std::min(a, b)/* TODO */;
    }
    BUILTIN(cl, 95, float, max, (float a, float b)) {
        return std::max(a, b)/* TODO */;
    }
    BUILTIN(cl, 96, float, bound, (float low, float x, float high)) {
        return std::min(std::max(low, x), high);
    }
    BUILTIN(cl, 97, float, pow, (float a, float b)) {
        return std::pow(a, b);
    }
    BUILTIN(cl, 98, entity, findfloat, (entity start, float edict_s::* fld, float match)) {
        return thisvm->entityManager.find(start, fld, match);
    }
    BUILTIN(cl, 99, float, checkextension, (string s)) {
        return 1/* TODO */;
    }
    BUILTIN(cl, 110, float, fopen, (string filename, float mode)) {
         auto fd = fileHandle_t {}; trap_FS_FOpenFile(filename->c_str(), &fd, mode == 2 ? fsMode_t::FS_WRITE : mode == 1 ? fsMode_t::FS_APPEND : fsMode_t::FS_READ ); return fd ;
    }
    BUILTIN(cl, 111, void, fclose, (float fhandle)) {
         trap_FS_FCloseFile(fhandle) ;
    }
    BUILTIN(cl, 112, string, fgets, (float fhandle)) {
         auto fd = fileHandle_t {static_cast<int>(fhandle)}; std::string ret; char c; for (;;) { trap_FS_Read(&c, 1, fd); if (c == 0 || c == '\n') break; ret += c; } return ret ;
    }
    BUILTIN(cl, 113, void, fputs, (float fhandle, string s)) {
        /* TODO */;
    }
    BUILTIN(cl, 114, float, strlen, (string s)) {
        return s->length();
    }
    BUILTIN(cl, 115, string, strcat, (Q1VM::Arguments args)) {
        std::string ret; for (int i = 0; i < args.size(); ++i) ret += args.get<string>(i); return ret;
    }
    BUILTIN(cl, 116, string, substring, (string s, float start, float length)) {
        try { return s->substr(start, length); } catch(std::out_of_range e) { return ""; };
    }
    BUILTIN(cl, 117, vector, stov, (string)) {
        return {0, 0, 0}/* TODO */;
    }
    BUILTIN(cl, 118, string, strzone, (string s)) {
        return s;
    }
    BUILTIN(cl, 119, void, strunzone, (string s)) {
        ;
    }
    BUILTIN(cl, 221, float, strstrofs, (string str, string sub, float startpos)) {
         const auto pos = str->find(sub, startpos); if (pos == std::string::npos) return -1; return pos;
    }
    BUILTIN(cl, 225, string, strpad, (float chars, string s)) {
        return s/* TODO */;
    }
    BUILTIN(cl, 226, string, infoadd, (string info, string key, string value)) {
        return ""/* TODO */;
    }
    BUILTIN(cl, 227, string, infoget, (string info, string key)) {
        return ""/* TODO */;
    }
    BUILTIN(cl, 228, float, strncmp, (string s1, string s2, float len)) {
        return s1->compare(s2) == 0/* TODO */;
    }
    BUILTIN(cl, 229, float, strcasecmp, (string s1, string s2)) {
        return s1->compare(s2) == 0/* TODO */;
    }
    BUILTIN(cl, 230, float, strncasecmp, (string s1, string s2, float len)) {
        return s1->compare(s2) == 0/* TODO */;
    }
    BUILTIN(cl, 277, float, frameduration, (float modlindex, float framenum)) {
        return 0/* TODO */;
    }
    BUILTIN(cl, 300, void, clearscene, ()) {
        /* TODO */;
    }
    BUILTIN(cl, 301, void, addentities, (float mask)) {
        /* TODO */;
    }
    BUILTIN(cl, 302, void, addentity, (entity ent)) {
        /* TODO */;
    }
    BUILTIN(cl, 303, float, setproperty, (float property, Q1VM::Arguments args)) {
        return View::setproperty(property, args);
    }
    BUILTIN(cl, 304, void, renderscene, ()) {
        /* TODO */;
    }
    BUILTIN(cl, 305, void, adddynamiclight, (vector org, float radius, vector lightcolours)) {
        /* TODO */;
    }
    BUILTIN(cl, 306, void, R_BeginPolygon, (string texturename, float flag)) {
        /* TODO */;
    }
    BUILTIN(cl, 307, void, R_PolygonVertex, (vector org, vector texcoords, vector rgb, float alpha)) {
        /* TODO */;
    }
    BUILTIN(cl, 308, void, R_EndPolygon, ()) {
        /* TODO */;
    }
    BUILTIN(cl, 309, float, getproperty, (float property)) {
        return 0/* TODO */;
    }
    BUILTIN(cl, 310, vector, cs_unproject, (vector v)) {
        return {0, 0, 0}/* TODO */;
    }
    BUILTIN(cl, 311, vector, cs_project, (vector v)) {
        return {0, 0, 0}/* TODO */;
    }
    BUILTIN(cl, 317, string, precache_pic, (string name, float trywad)) {
        return ""/* TODO */;
    }
    BUILTIN(cl, 318, vector, draw_getimagesize, (string picname)) {
        return {0, 0, 0}/* TODO */;
    }
    BUILTIN(cl, 321, float, drawstring, (vector position, string text, vector scale, vector rgb, float alpha, float flag)) {
        return 0/* TODO */;
    }
    BUILTIN(cl, 326, float, drawcolorcodedstring, (vector position, string text, vector scale, vector rgb, float alpha, float flag)) {
        return 0/* TODO */;
    }
    BUILTIN(cl, 327, float, stringwidth, (string text, float allowColorCodes, vector size)) {
        return 0/* TODO */;
    }
    BUILTIN(cl, 330, float, getstatf, (float stnum)) {
        return 0/* TODO */;
    }
    BUILTIN(cl, 331, float, getstati, (float stnum)) {
        return 0/* TODO */;
    }
    BUILTIN(cl, 339, void, print, (Q1VM::Arguments args)) {
        Log::Notice("%s", strcat(args)->c_str());
    }
    BUILTIN(cl, 346, void, setsensitivityscale, (float sens)) {
        /* TODO */;
    }
    BUILTIN(cl, 348, string, getplayerkeyvalue, (float playernum, string keyname)) {
        return ""/* TODO */;
    }
    BUILTIN(cl, 349, float, isdemo, ()) {
        return 0/* TODO */;
    }
    BUILTIN(cl, 350, float, isserver, ()) {
        return 0/* TODO */;
    }
    BUILTIN(cl, 352, void, registercommand, (string cmdname)) {
        /* TODO */;
    }
    BUILTIN(cl, 400, void, copyentity, (entity from, entity to)) {
        thisvm->entityManager.copyEntity(from, to);
    }
    BUILTIN(cl, 402, entity, findchain, (string edict_s::* fld, string match, Q1VM::Arguments args)) {
        return thisvm->entityManager.findChain(fld, match, (args.size() == 0) ? &edict_s::chain : args.get<int edict_s::*>(0));
    }
    BUILTIN(cl, 403, entity, findchainfloat, (float edict_s::* fld, float match, Q1VM::Arguments args)) {
        return thisvm->entityManager.findChain(fld, match, (args.size() == 0) ? &edict_s::chain : args.get<int edict_s::*>(0));
    }
    BUILTIN(cl, 442, string, argv, (float n)) {
        return ""/* TODO */;
    }
    BUILTIN(cl, 444, float, search_begin, (string pattern, float caseinsensitive, float quiet)) {
        return 0/* TODO */;
    }
    BUILTIN(cl, 445, void, search_end, (float handle)) {
        /* TODO */;
    }
    BUILTIN(cl, 446, float, search_getsize, (float handle)) {
        return 0/* TODO */;
    }
    BUILTIN(cl, 447, string, search_getfilename, (float handle, float num)) {
        return ""/* TODO */;
    }
    BUILTIN(cl, 448, string, cvar_string, (string s)) {
         char buf[256]; trap_Cvar_VariableStringBuffer(s->c_str(), buf, sizeof(buf)); std::string ret(buf, sizeof(buf)); return ret ;
    }
    BUILTIN(cl, 451, float, gettagindex, (entity ent, string tagname)) {
        return 0/* TODO */;
    }
    BUILTIN(cl, 459, entity, entitybyindex, (float num)) {
        return thisvm->entityManager.get(num).data;
    }
    BUILTIN(cl, 460, float, buf_create, ()) {
        return Buffer::New();
    }
    BUILTIN(cl, 461, void, buf_del, (float bufhandle)) {
        Buffer::Delete(static_cast<int>(bufhandle));
    }
    BUILTIN(cl, 462, float, buf_getsize, (float bufhandle)) {
        /* TODO */;
    }
    BUILTIN(cl, 463, void, buf_copy, (float bufhandle_from, float bufhandle_to)) {
        /* TODO */;
    }
    BUILTIN(cl, 464, void, buf_sort, (float bufhandle, float sortpower, float backward)) {
        /* TODO */;
    }
    BUILTIN(cl, 466, string, bufstr_get, (float bufhandle, float string_index)) {
        return "TODO"/* TODO */;
    }
    BUILTIN(cl, 467, void, bufstr_set, (float bufhandle, float string_index, string str)) {
        /* TODO */;
    }
    BUILTIN(cl, 468, float, bufstr_add, (float bufhandle, string str, float order)) {
        return 0/* TODO */;
    }
    BUILTIN(cl, 469, void, bufstr_free, (float bufhandle, float string_index)) {
        /* TODO */;
    }
    BUILTIN(cl, 471, float, asin, (float f)) {
        return std::asin(f);
    }
    BUILTIN(cl, 472, float, acos, (float f)) {
        return std::acos(f);
    }
    BUILTIN(cl, 473, float, atan, (float f)) {
        return std::atan(f);
    }
    BUILTIN(cl, 474, float, atan2, (float a, float b)) {
        return std::atan2(a, b);
    }
    BUILTIN(cl, 475, float, tan, (float f)) {
        return std::tan(f);
    }
    BUILTIN(cl, 479, float, tokenizebyseparator, (string s)) {
        return 0/* TODO */;
    }
    BUILTIN(cl, 480, string, strtolower, (string s)) {
         std::string ret; std::locale loc; for (auto i = 0u; i < s->length(); ++i) ret += std::tolower(s->at(i), loc); return ret;
    }
    BUILTIN(cl, 481, string, strtoupper, (string s)) {
         std::string ret; std::locale loc; for (auto i = 0u; i < s->length(); ++i) ret += std::toupper(s->at(i), loc); return ret;
    }
    BUILTIN(cl, 482, string, cvar_defstring, (string s)) {
         char buf[256]; trap_Cvar_VariableStringBuffer(s->c_str(), buf, sizeof(buf)); ;
    }
    BUILTIN(cl, 484, string, strreplace, (string search, string replace, string subject)) {
        return subject/* TODO */;
    }
    BUILTIN(cl, 485, string, strireplace, (string search, string replace, string subject)) {
        return subject/* TODO */;
    }
    BUILTIN(cl, 494, float, crc16, (float caseinsensitive, string s)) {
        return 0/* TODO */;
    }
    BUILTIN(cl, 495, float, cvar_type, (string name)) {
         const float CVAR_TYPEFLAG_EXISTS = BIT(0); const float CVAR_TYPEFLAG_SAVED = BIT(1); const float CVAR_TYPEFLAG_PRIVATE = BIT(2); const float CVAR_TYPEFLAG_ENGINE = BIT(3); const float CVAR_TYPEFLAG_HASDESCRIPTION = BIT(4); const float CVAR_TYPEFLAG_READONLY = BIT(5); /* TODO */ return CVAR_TYPEFLAG_EXISTS;
    }
    BUILTIN(cl, 496, float, numentityfields, ()) {
        return 0/* TODO */;
    }
    BUILTIN(cl, 503, string, whichpack, (string)) {
        return ""/* TODO */;
    }
    BUILTIN(cl, 510, string, uri_escape, (string in)) {
        return in/* TODO */;
    }
    BUILTIN(cl, 511, string, uri_unescape, (string in)) {
        return in/* TODO */;
    }
    BUILTIN(cl, 512, float, num_for_edict, (entity ent)) {
        return static_cast<float>(EntityManager::identify(ent));
    }
    BUILTIN(cl, 517, void, buf_cvarlist, (float buf, string prefix, string antiprefix)) {
        /* TODO */;
    }
    BUILTIN(cl, 519, float, gettime, (float timer)) {
        return 0/* TODO */;
    }
    BUILTIN(cl, 532, float, log, (float f)) {
        return std::log(f);
    }
    BUILTIN(cl, 627, string, sprintf, (string format, Q1VM::Arguments args)) {
         std::string ret; int i = 0; for (const char *s = format->c_str(); *s; ++s) { if (*s != '%') { ret += *s; } else { s += 1; if (*s == '%') { ret += '%'; } else if (*s == 's') { ret += args.get<string>(i++); } else if (*s == 'f') { ret += std::to_string(args.get<float>(i++)); } else {
} } } return ret;
    }
    BUILTIN(cl, 639, string, digest_hex, (string digest, string data)) {
        return data/* TODO */;
    }
#pragma GCC diagnostic pop
}
//[[[end]]] (checksum: 6e86d8529b234f6ab6136eec27edc71f)
