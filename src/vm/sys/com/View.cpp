#include "View.hpp"

namespace {
    const int VF_MIN = 1;    //(vector)
    const int VF_MIN_X = 2;    //(float)
    const int VF_MIN_Y = 3;    //(float)

    const int VF_SIZE = 4;    //(vector) (viewport size)
    const int VF_SIZE_Y = 5;    //(float)
    const int VF_SIZE_X = 6;    //(float)

    const int VF_VIEWPORT = 7;    //(vector, vector)

    const int VF_FOV = 8;    //(vector)
    const int VF_FOVX = 9;    //(float)
    const int VF_FOVY = 10;    //(float)

    const int VF_ORIGIN = 11;    //(vector)
    const int VF_ORIGIN_X = 12;    //(float)
    const int VF_ORIGIN_Y = 13;    //(float)
    const int VF_ORIGIN_Z = 14;    //(float)

    const int VF_ANGLES = 15;    //(vector)
    const int VF_ANGLES_X = 16;    //(float)
    const int VF_ANGLES_Y = 17;    //(float)
    const int VF_ANGLES_Z = 18;    //(float)

    const int VF_DRAWWORLD = 19;    //(float)

    const int VF_DRAWENGINESBAR = 20;    //(float)

    const int VF_DRAWCROSSHAIR = 21;    //(float)

    const int VF_CL_VIEWANGLES = 33;    //(vector)
    const int VF_CL_VIEWANGLES_X = 34;    //(float)
    const int VF_CL_VIEWANGLES_Y = 35;    //(float)
    const int VF_CL_VIEWANGLES_Z = 36;    //(float)

    const int VF_PERSPECTIVE = 200;

    const int VF_MAINVIEW = 400;

    const int VF_MINFPS_QUALITY = 401;
};

QVec3 View::origin = {0, 0, 0};

float View::setproperty(float property, Q1VM::Arguments arguments)
{
    switch ((int) property) {
        default:
            return 0;
        case VF_ORIGIN:
            origin = arguments.get<QVec3>(0);
            return true;
    }
}
