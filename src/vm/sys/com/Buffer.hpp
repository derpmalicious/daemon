#pragma once

#include <vector>

class Buffer {
public:
    static int New();

    static void Delete(int i);
};
