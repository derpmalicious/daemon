#pragma once

#include <vm/Q1VM.hpp>

class View {
public:
    static QVec3 origin;

    static float setproperty(float property, Q1VM::Arguments arguments);
};
