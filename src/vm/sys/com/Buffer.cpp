#include "Buffer.hpp"

static std::vector<Buffer *> allocated;

void Buffer::Delete(int i)
{
    delete allocated[i];
    allocated[i] = nullptr;
}

int Buffer::New()
{
    int i = 0;
    auto buffer = new Buffer;
    for (auto it : allocated) {
        if (it) {
            i++;
            continue;
        }
        allocated[i] = buffer;
        return i;
    }
    allocated.push_back(buffer);
    return i;
}
