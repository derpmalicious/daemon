#include "Q1VM.hpp"
#include <engine/client/cg_api.h>

#include "util/binparse.hpp"

using namespace binparse;

Q1VM *thisvm;

enum class Instruction : uint16_t;

struct Q1VM::ProgramData {
    struct Header {
        uint32_t version;
        uint32_t crc;

        struct Section {
            uint32_t offset;
            uint32_t count;

            static Section read(uint8_t *data)
            {
                auto it = Section();
                readSeek(it.offset, data);
                readSeek(it.count, data);
                return it;
            }
        };

        Section statements;
        Section globalDefs;
        Section fieldDefs;
        Section functions;
        Section stringData;
        Section globalData;
        uint32_t entityFields;

        static Header read(uint8_t *data)
        {
            auto it = Header();
            readSeek(it.version, data);
            readSeek(it.crc, data);
            readSeek(it.statements, data);
            readSeek(it.globalDefs, data);
            readSeek(it.fieldDefs, data);
            readSeek(it.functions, data);
            readSeek(it.stringData, data);
            readSeek(it.globalData, data);
            readSeek(it.entityFields, data);
            return it;
        }
    } header;

    struct Statement {
        Instruction id;
        uint16_t a, b, c;

        static Statement read(uint8_t *data)
        {
            auto it = Statement();
            readSeek((uint16_t &) it.id, data);
            readSeek(it.a, data);
            readSeek(it.b, data);
            readSeek(it.c, data);
            return it;
        }
    };

    std::vector<Statement> statements;

    struct Definition {
        uint16_t type;
        uint16_t offset;
        uint32_t nameOffset;

        static Definition read(uint8_t *data)
        {
            auto it = Definition();
            readSeek(it.type, data);
            readSeek(it.offset, data);
            readSeek(it.nameOffset, data);
            return it;
        }

        char const *name(ProgramData &data) const
        {
            return data.getString(nameOffset);
        }
    };

    std::vector<Definition> globalDefs;
    std::vector<Definition> fieldDefs;

    struct Function {
        int32_t firstStatement;
        uint32_t firstLocal;
        uint32_t numLocals;
        uint32_t profiling;
        uint32_t nameOffset;
        uint32_t fileNameOffset;
        uint32_t numParams;
        uint8_t sizes[8];

        static Function read(uint8_t *data)
        {
            auto it = Function();
            readSeek(it.firstStatement, data);
            readSeek(it.firstLocal, data);
            readSeek(it.numLocals, data);
            readSeek(it.profiling, data);
            readSeek(it.nameOffset, data);
            readSeek(it.fileNameOffset, data);
            readSeek(it.numParams, data);
            for (auto i = 0u; i < ARRAY_LEN(it.sizes); i++) {
                it.sizes[i] = *(data++);
            }
            return it;
        }

        char const *name(ProgramData &data) const
        {
            return data.getString(nameOffset);
        }
    };

    std::vector<Function> functions;
    uint8_t *globalData;

    const char *strings;

    char const *getString(size_t offset) const
    {
        if (offset > len) return "<OUT OF BOUNDS>";
        return &strings[offset];
    }

    size_t len;

    ProgramData(uint8_t *data, size_t len) : len{len}
    {
        header = Header::read(data);
        readList(statements, data + header.statements.offset, header.statements.count);
        readList(globalDefs, data + header.globalDefs.offset, header.globalDefs.count);
        readList(fieldDefs, data + header.fieldDefs.offset, header.fieldDefs.count);
        readList(functions, data + header.functions.offset, header.functions.count);
        globalData = data + header.globalData.offset;
        strings = (const char *) (data + header.stringData.offset);
    }
};

QStr StringManager::get(int32_t i) const
{
    if (i < 0) {
        i = -(i + 1);
        if ((size_t) i < temps.size()) {
            return temps[i];
        }
    }
    static auto oob = std::string("OUT OF BOUNDS");
    if ((size_t) i > vm.data.len - vm.data.header.stringData.offset) return oob;
    return &vm.data.strings[i];
}

Q1VM::Q1VM(type mode, builtinmap &builtins, uint8_t *buf, size_t len) :
        data{*new ProgramData{buf, len}},
        entityManager{*this, data.header.entityFields},
        mode{mode},
        m_builtins{builtins}
{
    memcpy(memory, data.globalData, sizeof(memory));
    globals = reinterpret_cast<globalvars_t *>(memory);
}

Q1VM *Q1VM::load(type mode, const char *filename)
{
    const auto MAX_PROGS_LENGTH = size_t{10 * 1024 * 1024};

    fileHandle_t f;
    const auto len_ = ssize_t{trap_FS_FOpenFile(va("%s", filename), &f, fsMode_t::FS_READ)};
    if (!f || len_ < 0) {
        Log::Warn("file not found: %s", filename);
        return nullptr;
    }
    size_t len = len_;
    if (len >= MAX_PROGS_LENGTH) {
        Log::Warn("file too large: %s is %li, max allowed is %li", filename, len, MAX_PROGS_LENGTH);
        trap_FS_FCloseFile(f);
        return nullptr;
    }
    auto buf = new uint8_t[MAX_PROGS_LENGTH + 1];
    trap_FS_Read(buf, std::min(len, MAX_PROGS_LENGTH), f);
    buf[len] = 0;
    trap_FS_FCloseFile(f);
    builtinmap *m = nullptr;
    switch (mode) {
        case Q1VM::type::sv:
            m = &builtins<Q1VM::type::sv>();
            break;
        case Q1VM::type::cl:
            m = &builtins<Q1VM::type::cl>();
            break;
        case Q1VM::type::ui:
            m = &builtins<Q1VM::type::ui>();
            break;
    }
    Q1VM *vm = new Q1VM{mode, *m, buf, len};
    vm->spawn();
    return vm;
}

void Q1VM::jumpcheck() {
    if (++jumpcount == 10000000) {
        Sys::Error("Runaway loop counter hit limit of %i jumps", jumpcount);
    }
}

#define TRUTHY(x) ((x) & (uint32_t(-1) >> 1))

#define INSTRUCTIONS(_)                                                     \
    _(DONE, {                                                               \
        f[OFS_PARAM(-1) + 0] = f[it.a + 0];                                 \
        f[OFS_PARAM(-1) + 1] = f[it.a + 1];                                 \
        f[OFS_PARAM(-1) + 2] = f[it.a + 2];                                 \
        return 0;                                                           \
    })                                                                      \
    _(MUL_FLOAT, { f[it.c] = f[it.a] * f[it.b]; })                          \
    _(MUL_VEC, {                                                            \
        f[it.c] = (f[it.a + 0] * f[it.b + 0]                                \
                +  f[it.a + 1] * f[it.b + 1]                                \
                +  f[it.a + 2] * f[it.b + 2]);                              \
    })                                                                      \
    _(MUL_FLOAT_VEC, {                                                      \
        const auto tmpf = f[it.a];                                          \
        f[it.c + 0] = tmpf * f[it.b + 0];                                   \
        f[it.c + 1] = tmpf * f[it.b + 1];                                   \
        f[it.c + 2] = tmpf * f[it.b + 2];                                   \
    })                                                                      \
    _(MUL_VEC_FLOAT, {                                                      \
        const auto tmpf = f[it.b];                                          \
        f[it.c + 0] = f[it.a + 0] * tmpf;                                   \
        f[it.c + 1] = f[it.a + 1] * tmpf;                                   \
        f[it.c + 2] = f[it.a + 2] * tmpf;                                   \
    })                                                                      \
    _(DIV_FLOAT, { f[it.c] = f[it.b] ? f[it.a] / f[it.b] : 0; })            \
    _(ADD_FLOAT, { f[it.c] = f[it.a] + f[it.b]; })                          \
    _(ADD_VEC, {                                                            \
        f[it.c + 0] = f[it.a + 0] + f[it.b + 0];                            \
        f[it.c + 1] = f[it.a + 1] + f[it.b + 1];                            \
        f[it.c + 2] = f[it.a + 2] + f[it.b + 2];                            \
    })                                                                      \
    _(SUB_FLOAT, { f[it.c] = f[it.a] - f[it.b]; })                          \
    _(SUB_VEC, {                                                            \
        f[it.c + 0] = f[it.a + 0] - f[it.b + 0];                            \
        f[it.c + 1] = f[it.a + 1] - f[it.b + 1];                            \
        f[it.c + 2] = f[it.a + 2] - f[it.b + 2];                            \
    })                                                                      \
    _(EQ_FLOAT, { f[it.c] = (f[it.a] == f[it.b]); })                        \
    _(EQ_VEC, {                                                             \
        f[it.c] = (f[it.a + 0] == f[it.b + 0]                               \
                && f[it.a + 1] == f[it.b + 1]                               \
                && f[it.a + 2] == f[it.b + 2]);                             \
    })                                                                      \
    _(EQ_STR, { f[it.c] = (*s(it.a) == *s(it.b)); })                        \
    _(EQ_ENT, { f[it.c] = (i[it.a] == i[it.b]); })                          \
    _(EQ_FUNC, { f[it.c] = (i[it.a] == i[it.b]); })                         \
    _(NE_FLOAT, { f[it.c] = (f[it.a] != f[it.b]); })                        \
    _(NE_VEC, {                                                             \
        f[it.c] = (f[it.a + 0] != f[it.b + 0]                               \
                || f[it.a + 1] != f[it.b + 1]                               \
                || f[it.a + 2] != f[it.b + 2]);                             \
    })                                                                      \
    _(NE_STR, { f[it.c] = (*s(it.a) != *s(it.b)); })                        \
    _(NE_ENT, { f[it.c] = (i[it.a] != i[it.b]); })                          \
    _(NE_FUNC, { f[it.c] = (i[it.a] != i[it.b]); })                         \
    _(LE, { f[it.c] = (f[it.a] <= f[it.b]); })                              \
    _(GE, { f[it.c] = (f[it.a] >= f[it.b]); })                              \
    _(LT, { f[it.c] = (f[it.a] < f[it.b]); })                               \
    _(GT, { f[it.c] = (f[it.a] > f[it.b]); })                               \
    _(LOAD_FLOAT, { f[it.c] = e.read<float>(i[it.a], i[it.b]); })           \
    _(LOAD_VEC, {                                                           \
        const auto v = e.read<QVec3>(i[it.a], i[it.b]);                     \
        f[it.c + 0] = v.x;                                                  \
        f[it.c + 1] = v.y;                                                  \
        f[it.c + 2] = v.z;                                                  \
    })                                                                      \
    _(LOAD_STR, { i[it.c] = e.read<int>(i[it.a], i[it.b]); })               \
    _(LOAD_ENT, { i[it.c] = e.read<int>(i[it.a], i[it.b]); })               \
    _(LOAD_FIELD, { i[it.c] = e.read<int>(i[it.a], i[it.b]); })             \
    _(LOAD_FUNC, { i[it.c] = e.read<int>(i[it.a], i[it.b]); })              \
    _(ADDRESS, { i[it.c] = e.address(i[it.a], i[it.b]); })                  \
    _(STORE_FLOAT, { f[it.b] = f[it.a]; })                                  \
    _(STORE_VEC, {                                                          \
        f[it.b + 0] = f[it.a + 0];                                          \
        f[it.b + 1] = f[it.a + 1];                                          \
        f[it.b + 2] = f[it.a + 2];                                          \
    })                                                                      \
    _(STORE_STR, { f[it.b] = f[it.a]; })                                    \
    _(STORE_ENT, { f[it.b] = f[it.a]; })                                    \
    _(STORE_FIELD, { f[it.b] = f[it.a]; })                                  \
    _(STORE_FUNC, { f[it.b] = f[it.a]; })                                   \
    _(STOREP_FLOAT, { e.write(i[it.b], f[it.a]); })                         \
    _(STOREP_VEC, {                                                         \
        e.write(i[it.b], QVec3{f[it.a + 0], f[it.a + 1], f[it.a + 2]});     \
    })                                                                      \
    _(STOREP_STR, { e.write(i[it.b], i[it.a]); })                           \
    _(STOREP_ENT, { e.write(i[it.b], i[it.a]); })                           \
    _(STOREP_FIELD, { e.write(i[it.b], i[it.a]); })                         \
    _(STOREP_FUNC, { e.write(i[it.b], i[it.a]); })                          \
    _(RETURN, {                                                             \
        f[OFS_PARAM(-1) + 0] = f[it.a + 0];                                 \
        f[OFS_PARAM(-1) + 1] = f[it.a + 1];                                 \
        f[OFS_PARAM(-1) + 2] = f[it.a + 2];                                 \
        return 0;                                                           \
    })                                                                      \
    _(NOT_FLOAT, { f[it.c] = !TRUTHY(i[it.a]); })                           \
    _(NOT_VEC, {                                                            \
        f[it.c] = (!f[it.a + 0]                                             \
                && !f[it.a + 1]                                             \
                && !f[it.a + 2]);                                           \
    })                                                                      \
    _(NOT_STR, { f[it.c] = (!i[it.a] || s(it.a)->empty()); })               \
    _(NOT_ENT, { f[it.c] = (!i[it.a]); })                                   \
    _(NOT_FUNC, { f[it.c] = (!i[it.a]); })                                  \
    _(IF, { vm.jumpcheck(); return TRUTHY(i[it.a]) ? (int16_t) it.b : 1; }) \
    _(IFNOT, { vm.jumpcheck(); return !TRUTHY(i[it.a]) ? (int16_t) it.b : 1; }) \
    _(CALL0, {})                                                            \
    _(CALL1, {})                                                            \
    _(CALL2, {})                                                            \
    _(CALL3, {})                                                            \
    _(CALL4, {})                                                            \
    _(CALL5, {})                                                            \
    _(CALL6, {})                                                            \
    _(CALL7, {})                                                            \
    _(CALL8, {})                                                            \
    _(STATE, {})                                                            \
    _(GOTO, { vm.jumpcheck(); return static_cast<int16_t>(it.a); })         \
    _(AND, { f[it.c] = (TRUTHY(i[it.a]) && TRUTHY(i[it.b])); })             \
    _(OR,  { f[it.c] = (TRUTHY(i[it.a]) || TRUTHY(i[it.b])); })             \
    _(BITAND, { f[it.c] = (static_cast<int>(f[it.a]) & static_cast<int>(f[it.b])); }) \
    _(BITOR,  { f[it.c] = (static_cast<int>(f[it.a]) | static_cast<int>(f[it.b])); }) \
/**/

enum class Instruction : uint16_t {
#define X(id, code) id,
    INSTRUCTIONS(X)
#undef X
};

static int16_t exec(Q1VM &vm, Q1VM::ProgramData::Function const &function, Q1VM::ProgramData::Statement const &it)
{
    auto f = (float *) vm.memory;
    auto i = (int *) vm.memory;
#define s(_) vm.stringManager.get(i[_])
    auto &e = vm.entityManager;
    auto g = [&](uint16_t _) -> char const * {
        auto iv = i[_];
        auto fv = f[_];
        auto extra = "";
        char const *missing = "";
#define first(v) std::find_if(std::begin(v), std::end(v), \
            [&] (Q1VM::ProgramData::Definition d){ return d.offset == _; })
        auto global = first(vm.data.globalDefs);
        auto field = first(vm.data.fieldDefs);
#undef first
        const char *func = (iv > 0 && (size_t) iv < vm.data.functions.size())
                           ? vm.data.functions[iv].name(vm.data)
                           : missing;
        extra = va("|%s|.%s|%s()|\"%s\"",
                   global != std::end(vm.data.globalDefs) ? global->name(vm.data) : missing,
                   field != std::end(vm.data.fieldDefs) ? field->name(vm.data) : missing,
                   func,
                   vm.stringManager.get(iv)->c_str()
        );
        return va("<$%i: %i|%f%s>", _, iv, fv, extra);
    };
    switch (it.id) {
#define X(id, code) \
        case Instruction::id: \
            Log::defaultLogger.DoDebugCode([&]() { Log::Debug("%-16s %-13s %-60s %-60s %-60s", function.name(vm.data), #id, g(it.a), g(it.b), g(it.c)); });  \
            code \
            break;
        INSTRUCTIONS(X)
#undef X
    };
#undef s
    return 1;
}

static const char *vmnames[] = {"SV", "CL", "Menu"};

static void exec(Q1VM *vmptr, Q1VM::ProgramData::Function const &start)
{
    auto &vm = *vmptr;
    class Frame {
        std::vector<uint32_t> locals;
        Q1VM &vm;
    public:
        const Q1VM::ProgramData::Function &func;
        const int comeFrom;

        Frame(Q1VM &vm, const Q1VM::ProgramData::Function &func, const int comeFrom)
                : vm{vm}, func{func}, comeFrom{comeFrom}
        {
            locals.resize(func.numLocals);
            auto src = func.firstLocal;
            for (auto i = 0u; i < func.numLocals; i++) {
                locals[i] = vm.memory[src++];
            }
            // inject locals
            auto dst = func.firstLocal;
            for (auto param = 0u; param < func.numParams; param++) {
                for (auto ofs = 0u; ofs < func.sizes[param]; ofs++) {
                    vm.memory[dst++] = vm.memory[(OFS_PARAM(param) + ofs)];
                }
            }
        }

        Frame(Q1VM &vm, int i, const int comeFrom) : Frame{vm, vm.data.functions[i], comeFrom}
        { }

        const char *name = func.name(vm.data);

        ~Frame()
        {
            auto dst = func.firstLocal;
            for (auto src = 0u; src < locals.size(); src++) {
                vm.memory[dst++] = locals[src];
            }
        }
    };
    auto stack = std::stack<Frame>{{{vm, start, -1}}};
    auto stmtIdx = start.firstStatement;
    Log::Debug("[Q1VM] > %s", start.name(vm.data));
    while (!stack.empty()) {
        const auto &func = stack.top().func;
        const auto stmt = vm.data.statements[stmtIdx];
        const auto skip = exec(vm, func, stmt);
        if (skip) {
            stmtIdx += skip;
        } else {
            auto &it = stack.top();
            Log::Debug("[Q1VM] < %s", func.name(vm.data));
            stmtIdx = it.comeFrom;
            stack.pop();
            continue;
        }
        if ((int) stmt.id >= (int) Instruction::CALL0 && (int) stmt.id <= (int) Instruction::CALL8) {
            int nargs = Util::ordinal(stmt.id) - Util::ordinal(Instruction::CALL0);
            auto i = (int *) vm.memory;
            int iv = i[stmt.a];
            if (iv < 0 || (size_t) iv >= vm.data.functions.size()) {
                Log::Warn("[Q1VM] %s function %i out of bounds", vmnames[Util::ordinal(vm.mode)], iv);
                goto teardown;
            }
            if (iv == 0) {
                Log::Warn("[Q1VM] calling null function");
            }
            const auto &nextFunc = vm.data.functions[iv];
            const auto nextStmt = nextFunc.firstStatement;
            if (nextStmt < 0) {
                const auto builtin = -nextStmt;
                if (!vm.call(builtin, nargs)) {
                    Log::Warn("[Q1VM] %s builtin #%i not implemented", vmnames[Util::ordinal(vm.mode)], builtin);
                    continue;
                }
            } else {
                Log::Debug("[Q1VM] > %s", nextFunc.name(vm.data));
                stack.emplace(vm, iv, stmtIdx);
                stmtIdx = nextStmt;
            }
        }
    }
    teardown:
    {
        vmptr->jumpcount = 0;
        vmptr->stringManager.clear();
    }
}

bool Q1VM::call(int builtin, int nargs)
{
    const auto f = m_builtins[builtin];
    if (!f) return false;
    this->nargs = nargs;
    f(*this);
    return true;
}

void Q1VM::exec(int globalvars_t::*needle)
{
    ::exec(this, data.functions[globalVars.*needle]);
}

bool Q1VM::exec(char const *needle)
{
    for (const auto &it : data.functions) {
        if (strcmp(needle, it.name(data)) == 0) {
            ::exec(this, it);
            return true;
        }
    }
    return false;
}
