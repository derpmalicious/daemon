#pragma once

#include "String.hpp"
#include <vector>

class Q1VM;

class StringManager {
public:
    StringManager(Q1VM &vm);

    QStr get(int i) const;

    int32_t add(QStr s);

    void clear();

private:
    Q1VM &vm;
    std::vector<QStr> temps;
};
