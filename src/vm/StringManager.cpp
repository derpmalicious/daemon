#include "StringManager.hpp"

StringManager::StringManager(Q1VM &vm) : vm{vm}
{ }

int32_t StringManager::add(QStr s)
{
    // TODO: intern strings
    const int bt = 0;
    const auto sz = static_cast<int32_t>(temps.size());
    for (int i = 0; i < std::min(bt, sz); ++i) {
        if (*s == *temps[sz - i - 1]) {
            return -(1 + sz - i);
        }
    }
    temps.push_back(s);
    return -(1 + sz);
}

void StringManager::clear()
{
    temps.clear();
}
