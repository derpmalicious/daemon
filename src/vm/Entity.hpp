#pragma once

#include <memory>
#include <string>

using edict_s = struct entvars_t;

class Entity {
public:
    edict_s &data;
    uint32_t id;

    Entity(uint32_t id, edict_s &data);
};
