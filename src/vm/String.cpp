#include "String.hpp"

QStr::QStr(std::string const *s) : s{std::make_shared<std::string const>(*s)}
{ }

QStr::QStr(std::string const s) : s{std::make_shared<std::string const>(s)}
{ }

QStr::QStr(char const *c) : s{std::make_shared<std::string const>(c)}
{ }

QStr::operator std::shared_ptr<std::string const>() const
{ return s; }

std::string const &QStr::operator*() const
{ return *s; }

QStr::operator std::string const &() const
{ return *s; }

std::shared_ptr<std::string const> QStr::operator->() const
{ return s; }
