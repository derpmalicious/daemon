#include <util/meta.hpp>
#include <common/Common.h>
#include "EntityManager.hpp"
#include "Q1VM.hpp"

EntityManager::EntityManager(Q1VM &vm, size_t entsize) : vm{vm}, entsize{entsize}
{ }

namespace {
    const auto pwords = sizeof(Entity *) / sizeof(uint32_t);
};

Entity EntityManager::spawn()
{
    auto len = pwords + entsize;
    auto buf = new uint32_t[len];
    std::fill(buf, buf + len, 0);
    auto self = reinterpret_cast<Entity **>(buf);
    auto data = reinterpret_cast<edict_s *>(buf + pwords);
    auto e = *self = new Entity{autoid++, *data};
    entities.push_back(e);
    return *e;
}

uint32_t EntityManager::identify(edict_s &data)
{
    const auto buf = reinterpret_cast<uint32_t *>(&data) - pwords;
    const auto self = *reinterpret_cast<Entity **>(buf);
    return self->id;
}

void EntityManager::remove(edict_s &data)
{
    const auto buf = reinterpret_cast<uint32_t *>(&data) - pwords;
    delete buf;
}

Entity EntityManager::get(uint32_t ent) const
{
    return *entities[ent];
}

size_t EntityManager::address(uint32_t ent, int fld) const
{
    return ent * entsize + fld;
}

uint32_t *EntityManager::deref(size_t address) const
{
    auto ent = address / entsize;
    auto fld = address % entsize;
    auto &e = (ent < entities.size()) ? entities[ent] : entities[0];
    return &reinterpret_cast<uint32_t *>(&e->data)[fld];
}

edict_s &EntityManager::next(edict_s &e)
{
    const auto begin = EntityManager::identify(e);
    const auto end = begin + 1;
    const auto limit = entities.size();
    return get((end < limit) ? end : 0).data;
}

edict_s &EntityManager::find(edict_s &start, float edict_s::* fld, float match)
{
    for (edict_s *it = &start; EntityManager::identify(*(it = &next(*it)));) {
        if (it->*fld == match) return *it;
    }
    return get(0).data;
}

edict_s &EntityManager::find(edict_s &start, int edict_s::* fld, QStr match)
{
    const auto &string = *match;
    for (edict_s *it = &start; EntityManager::identify(*(it = &next(*it)));) {
        if (*vm.stringManager.get(it->*fld) == string) return *it;
    }
    return get(0).data;
}

edict_s &EntityManager::findChain(float edict_s::* fld, float match, int edict_s::* chain)
{
    edict_s *prev = &get(0).data, *it = prev;
    for (; EntityManager::identify(*(it = &next(*it)));) {
        if (it->*fld == match) {
            it->*chain = EntityManager::identify(*prev);
            prev = it;
        }
    }
    return *prev;
}

edict_s &EntityManager::findChain(QStr edict_s::* fld, QStr match, int edict_s::* chain)
{
    const auto &string = *match;
    edict_s *prev = &get(0).data, *it = prev;
    for (; EntityManager::identify(*(it = &next(*it)));) {
        if (*(it->*fld) == string) {
            it->*chain = EntityManager::identify(*prev);
            prev = it;
        }
    }
    return *prev;
}

void EntityManager::copyEntity(edict_s &from, edict_s &to)
{
    memcpy(&to, &from, entsize * sizeof(uint32_t));
}
