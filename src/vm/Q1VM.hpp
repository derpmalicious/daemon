#pragma once

#include "vm/Entity.hpp"
#include "vm/EntityManager.hpp"
#include "vm/String.hpp"
#include "vm/StringManager.hpp"
#include "vm/Vector.hpp"

#include "util/meta.hpp"

#include <functional>
#include <map>
#include <memory>
#include <string>
#include <vector>

class Q1VM;

extern Q1VM *thisvm;

struct globalvars_t;

class Q1VM {
public:
    struct ProgramData;
    ProgramData &data;
    EntityManager entityManager;
    StringManager stringManager{*this};

    enum class type {
        sv,
        cl,
        ui
    };

    type mode;

    uint32_t memory[0xFFFF];
    globalvars_t &globalVars = *reinterpret_cast<globalvars_t *>(&memory);

    Entity spawn()
    {
        return entityManager.spawn();
    }

    using callable = void (*)(Q1VM &);

    using builtinmap = std::array<callable, 1024>;

    const builtinmap &m_builtins;

    template<type t>
    static builtinmap &builtins()
    {
        static builtinmap it;
        return it;
    };

    static Q1VM *load(type mode, const char *filename);

    void exec(int globalvars_t::*needle);

    bool exec(char const *needle);

    int jumpcount;
    void jumpcheck();

    template<class>
    struct wrapper;

    template<class>
    friend
    struct wrapper;

    template<type t, int i, callable f>
    struct Builtin {
        Builtin()
        { Q1VM::builtins<t>()[i] = f; }
    };

    bool call(int builtin, int nargs);

    struct Arguments {
        const Q1VM &vm;
        const int ofs;

        Arguments(const Q1VM &vm, int ofs) : vm{vm}, ofs{ofs}
        { }

        int size()
        { return vm.nargs; }

        template<class T>
        T get(int i);
    };

    template<class T>
    T get(int i) const;

    template<class T>
    T set(int i, T value);

private:
    globalvars_t *globals;
    int nargs;

    Q1VM(type mode, builtinmap &builtins, uint8_t *buf, size_t len);

    friend struct Arguments;

    template<class R, class... Args>
    void call(R (&f)(Args...));

    template<class... Args>
    void call(void (&f)(Args...));
};

#define Q1VM_MAKE_BUILTIN(fun) &Q1VM::wrapper<typeof(fun)>::call<fun>

template<class R, class... Args>
struct Q1VM::wrapper<R(Args...)> {
    template<R (&f)(Args...)>
    static void call(Q1VM &it)
    {
        thisvm = &it;
        it.call(f);
    };
};

constexpr int OFS_PARAM(int n)
{ return 4 + (n) * 3; }

template<class R, class... Args>
void Q1VM::call(R (&f)(Args...))
{
    auto i = -1;
    auto next = [&]() { return OFS_PARAM(++i); };
    (void) next;
    set<R>(OFS_PARAM(-1), meta::explode(f, std::tuple<Args...> {get<Args>(next())...}));
}

template<class... Args>
void Q1VM::call(void (&f)(Args...))
{
    auto i = -1;
    auto next = [&]() { return OFS_PARAM(++i); };
    (void) next;
    meta::explode(f, std::tuple<Args...> {get<Args>(next())...});
}

template<class T, class = void>
struct Q1VMMarshaling {
    /** guest -> host */
    static T get(const Q1VM &vm, int i);

    /** host -> guest */
    static T set(Q1VM &vm, int i, T value);
};

template<class T>
struct Q1VMMarshaling<T> {
    static T get(const Q1VM &vm, int i)
    {
        using fetch = typename std::add_const<
                typename std::remove_reference<T>::type
        >::type;
        return *reinterpret_cast<fetch *>(&vm.memory[i]);
    }

    static T set(Q1VM &vm, int i, T value)
    {
        return *reinterpret_cast<T *>(&vm.memory[i]) = value;
    }
};

template<class T, class C>
struct Q1VMMarshaling<T C::*> {
    using Self = T C::*;

    static Self get(const Q1VM &vm, int i)
    {
        return meta::offset_to_member<T, C>(vm.memory[i] * sizeof(uint32_t));
    }
};

template<>
struct Q1VMMarshaling<Q1VM::Arguments> {
    using Self = Q1VM::Arguments;

    static Self get(const Q1VM &vm, int i)
    {
        return Q1VM::Arguments(vm, i);
    }
};

template<>
struct Q1VMMarshaling<edict_s &> {
    using Self = edict_s &;

    static Self get(const Q1VM &vm, int i)
    {
        return vm.entityManager.get(vm.memory[i]).data;
    }

    static Self set(Q1VM &vm, int i, Self value)
    {
        vm.set(i, EntityManager::identify(value));
        return value;
    }
};

template<>
struct Q1VMMarshaling<QStr> {
    using Self = QStr;

    static Self get(const Q1VM &vm, int i)
    {
        return vm.stringManager.get(static_cast<int32_t>(vm.memory[i]));
    }

    static Self set(Q1VM &vm, int i, Self value)
    {
        vm.set(i, vm.stringManager.add(value));
        return value;
    }
};

template<class T>
T Q1VM::get(int i) const
{
    return Q1VMMarshaling<T>::get(*this, i);
}

template<class T>
T Q1VM::set(int i, T value)
{
    return Q1VMMarshaling<T>::set(*this, i, value);
}

template<class T>
T Q1VM::Arguments::get(int i)
{
    return vm.get<T>(ofs + (OFS_PARAM(i) - OFS_PARAM(0)));
}
