#pragma once

#include <engine/client/cg_api.h>
#include <engine/qcommon/q_shared.h>

#define OVERLOAD_(F, _9, _8, _7, _6, _5, _4, _3, _2, _1, n, ...) F##_##n
#define OVERLOAD(F, ...) OVERLOAD_(F,__VA_ARGS__,9,8,7,6,5,4,3,2,1)(__VA_ARGS__)

#define AUTOCVAR(...) OVERLOAD(AUTOCVAR, __VA_ARGS__)
#define AUTOCVAR_2(type, name)          cvar<type> name(#name)
#define AUTOCVAR_3(type, name, initial) cvar<type> name(#name, initial)

class Cvars {
    Cvars()
    { }

    typedef std::function<void()> func;
    std::vector<func> inits;
    std::vector<func> updates;
public:
    static Cvars &instance()
    {
        static Cvars it;
        return it;
    }

    void init(func f)
    {
        inits.push_back(f);
    }

    static void init()
    {
        auto &inst = instance();
        for (auto &f : inst.inits) {
            f();
        }
    }

    void update(func f)
    {
        updates.push_back(f);
    }

    static void update()
    {
        auto &inst = instance();
        for (auto &f : inst.updates) {
            f();
        }
    }
};

template<class T>
inline std::string stringify(T it)
{ return std::to_string(it); }

template<>
inline std::string stringify<char const *>(char const *c_str)
{ return std::string(c_str); }

template<class T>
class cvar {
    vmCvar_t inner;
    const char *name;
    T value;
    int modificationCount;
public:
    operator T()
    { return value; }

    T &operator=(T t)
    { return value = t; }

    cvar(char const *name_, T initial = T{}) : value{initial}, name{name_}
    {
        auto &cvars = Cvars::instance();
        cvars.init(std::function<void()>([&]() {
            const auto s = (new std::string(stringify(value)))->c_str(); // FIXME: leaking memory for trap_Cvar_Register
            trap_Cvar_Register(&inner, name, s, 0);
            modificationCount = inner.modificationCount;
        }));
        cvars.update(std::function<void()>([&]() {
            trap_Cvar_Update(&inner);
            if (modificationCount != inner.modificationCount) {
                modificationCount = inner.modificationCount;
                Com_Printf("%s: %s -> %s\n", name, stringify(value).c_str(), inner.string);
                // TODO: update value
            }
        }));
    }
};
