#include <engine/qcommon/q_shared.h>
#include <shared/VMMain.h>
#include <engine/server/sg_api.h>
#include "commands.hpp"

int VM::VM_API_VERSION = GAME_API_VERSION;

void CompleteCommand(int argc)
{
    Q_UNUSED(argc);
}

bool ConsoleCommand()
{
    char buffer[MAX_STRING_CHARS];
    trap_Argv(0, buffer, sizeof(buffer));
    Commands::instance().execute(buffer);
    return true;
}
